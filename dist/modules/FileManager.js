import fs from 'fs';
import User from './User.js';
class FileManager {
    constructor() {
        // the methods below use simple JSON storage to make sure data is preserved if the server shuts down
        // this reads all Item data from a .json file on startup
        this.readItems = () => {
            try {
                const fileData = fs.readFileSync("./src/data/saved-data.json");
                let items = JSON.parse(fileData.toString());
                //console.log("File data parsed: " + JSON.stringify(items));
                return items;
            }
            catch (error) {
                console.log("Error reading items file: " + error.message);
                return null;
            }
        };
        /**
         * This saves server data to a JSON file so it persists if the server is shut down.
         * @param items The collection if items stored by the server to be saved.
         */
        this.saveItems = (items) => {
            const itemsText = JSON.stringify(items);
            const fileStore = fs.writeFile("./src/data/saved-data.json", itemsText, err => {
                if (err) {
                    console.log("Error: " + err.message);
                }
                else {
                    console.log("File updated successfully.");
                }
            });
        };
        this.saveOrders = (orders) => {
            const ordersText = JSON.stringify(orders);
            const fileStore = fs.writeFile("./src/data/saved-orders.json", ordersText, err => {
                if (err) {
                    console.log("Error saving Orders: " + err.message);
                }
                else {
                    console.log("Orders file updated successfully.");
                }
            });
        };
        this.readOrders = () => {
            try {
                const fileData = fs.readFileSync("./src/data/saved-orders.json");
                let orders = JSON.parse(fileData.toString());
                console.log("Order data parsed: " + JSON.stringify(orders));
                return orders;
            }
            catch (error) {
                console.log("Error reading Orders file: " + error.message);
                return [];
            }
        };
        this.saveUsers = (users) => {
            const usersText = JSON.stringify(users);
            const fileStore = fs.writeFile("./src/data/saved-users.json", usersText, err => {
                if (err) {
                    console.log("Error saving Users: " + err.message);
                }
                else {
                    console.log("Users file updated successfully.");
                }
            });
        };
        this.readUsers = () => {
            try {
                const fileData = fs.readFileSync("./src/data/saved-users.json");
                let usersFile = JSON.parse(fileData.toString());
                console.log("Users data parsed: " + JSON.stringify(usersFile));
                // we need to make sure the Users are proper User objects and not just data with no methods
                let processedUsers = [];
                for (let i = 0; i < usersFile.length; i++) {
                    let nextUserData = usersFile[i];
                    let newUser = new User(nextUserData.username, nextUserData.password, nextUserData.email, false, true);
                    processedUsers.push(newUser);
                }
                return processedUsers;
            }
            catch (error) {
                console.log("Error reading Users file: " + error.message);
                return [];
            }
        };
        this.removeImage = (path) => {
            const removed = fs.unlink(path, err => {
                if (err) {
                    console.log(`Error removing image file ${path}`);
                }
                else {
                    console.log(`Image at ${path} removed.`);
                }
            });
        };
        // loads the Admin account saved as JSON on server startup
        this.loadAdmin = () => {
            let fileData = "";
            try {
                fileData = fs.readFileSync("./src/data/admin-accounts.json").toString();
            }
            catch (err) {
                console.log("Error: " + err.message);
            }
            if (fileData !== "") {
                let adminData = JSON.parse(fileData);
                let admin = new User(adminData.username, adminData.password, adminData.email, adminData.isAdmin, true);
                console.log("Admin data parsed.");
                return admin;
            }
        };
    }
}
export default FileManager;
//# sourceMappingURL=FileManager.js.map