import SiteController from "../dist/modules/SiteController.js";
import Item from "../dist/modules/Item.js";

describe("Site Controller:\n", () => {

    let controller = SiteController.getInstance();

    it("Should allow the Admin to log in", function() {
        expect(controller.loginAdmin("Yasha", "test")).toBeTruthy();
    })

    describe("Item tests:\n", () => {
        it("Should allow the user to get all Items", function() {
            expect(controller.getAllItems()).toEqual('[{"id":"905824234","height":"sdf","itemName":"Test 1","itemType":"Lacquered Box","price":"340","imageSrc":"20180719_155037.jpg"},{"id":"905824235","height":"sdf","itemName":"Test 2","itemType":"Lacquered Box","price":"349","imageSrc":"20180719_155046.jpg"},{"id":"905824237","height":"sdf","itemName":"Test 4","itemType":"Lacquered Box","price":"349","imageSrc":"20191129_103333.jpg"},{"id":"783453874","height":"4 in","itemName":"Test 3","itemType":"Birch Bark Box","price":"25","imageSrc":"NapoleonSmall.jpg"},{"id":"8923478","height":"3 in","itemName":"Test 5","itemType":"Lacquered Box","price":"49","imageSrc":"20180720_192628.jpg"}]');
        })

        it("Should allow adding a new item", function() {
            expect(controller.addItem("12345", "6 in", "Test Doll", "Nesting Doll", "35", "")).toBeTruthy();
        })

        it("Should allow the user to get one Item", function() {
            let newItem = new Item("12345", "6 in", "Test Doll", "Nesting Doll", "35", "");
            expect(controller.getItem("12345")).toEqual(newItem);
        })

        it("Should allow deleting a new item", function() {
            expect(controller.removeItem("12345")).toBeTruthy();
        })

    })

    
})