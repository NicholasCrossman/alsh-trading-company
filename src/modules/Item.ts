
type ItemType = 
    | "Nesting Doll" 
    | "Christmas Nesting Doll" 
    | "Wooden Santa" 
    | "Lacquered Box" 
    | "Birch Bark Box" 
    | "Ornament" 
    | "Icons and Religious" 
    | "Jewelry" 
    | "Souvenirs";

class Item {

    id: string;
    height: string;
    itemName: string;
    itemType: ItemType;
    price: number;
    imageSrc: string;

    /**
     * Constructor for the Item object, which holds an item's name, type, and price, 
     * as well as the URL to an image of the item.
     * @param id String - The unique ID of the item, used as the key to access the item.
     * @param height String - The height of the item, in inches.
     * @param itemName String - The Item's unique name.
     * @param itemType String - The Item's type.
     * @param price Number - The Item's price before tax.
     * @param imageSrc String - The URL to where the Item's image is held on this server.
     */
    constructor(id: string, height: string, itemName: string, itemType: string, price: number, imageSrc: string) {
        this.id = id;
        this.height = height;
        this.itemName = itemName;
        this.itemType = itemType as ItemType;
        this.price = price;
        this.imageSrc = imageSrc;
    }
}

export default Item;
