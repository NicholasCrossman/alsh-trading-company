window.addEventListener("load", () => {
    refreshItems();
});
/**
 * GET call that fetches and displays all items from the server.
 */
const refreshItems = async () => {
    const location = window.location.hostname;
    const response = await fetch(`http://${location}:3000/items`);
    let items = await response.json();
    items = JSON.parse(items);
    console.log("Items: " + items);
    var itemData = document.getElementById("item-data");
    var txt = "";
    if (items !== null) {
        for (var i = 0; i < items.length; i++) {
            txt += buildItem(items[i]);
        }
        itemData.innerHTML = txt;
    }
};
/**
 * Private method that turns a JSON Item object into an HTML tile to be displayed
 * on screen. The tile has a dropdown menu with additional info on the item, accessed
 * by hovering over the image name.
 * @param {*} item
 * @param {*} index
 */
const buildItem = (item) => {
    var txt = "";
    txt += "<div class='item'>";
    txt += "<img src='./public/images/" + item.imageSrc + "' />";
    // below is the code for a dropdown menu for the rest of the Item's data
    txt += "<div class='dropdown'>";
    txt += "<p>" + item.itemName + "</p>";
    txt += "<div class='dropdown-content'>";
    txt += "<p>ID:" + item.id + "</p>";
    txt += "<p>Type: " + item.itemType + "</p>";
    txt += "<p>Price: " + item.price + "</p>";
    txt += "<p>Height:" + item.height + "</p>";
    txt += "</div></div></div>";
    return txt;
};
// Login form code
// this sends the login form data asynchronously without redirecting the page
window.addEventListener('load', () => {
    const adminLogin = async () => {
        const loginData = new FormData(loginForm);
        const location = window.location.hostname;
        // console.log("Form data:");
        // for(var pair of loginData.entries()) {
        //     console.log(`Key: ${pair[0]}, Value: ${pair[1]}`);
        // }
        let data = {
            username: loginData.get("username"),
            password: loginData.get("password")
        };
        let jsonData = JSON.stringify(data);
        const response = await fetch(`http://${location}:3000/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: jsonData
        });
        const status = response.status;
        if (status !== 200) {
            console.log(`Error: status code ${status}`);
        }
        else {
            console.log(`Success: status code ${status}`);
            const token = await response.json();
            console.log("Token recieved: " + token.adminToken);
        }
    };
    //override the form's submit method
    const loginForm = document.getElementById("login-form");
    loginForm.addEventListener("submit", (event) => {
        event.preventDefault();
        adminLogin();
    });
});
// Create Item form code
// this sends the Item creation form data asynchronously without redirecting the page
window.addEventListener('load', () => {
    const createItem = async () => {
        const itemData = new FormData(itemForm);
        const location = window.location.hostname;
        const response = await fetch(`http://${location}:3000/items`, {
            method: "POST",
            body: itemData
        });
        const status = response.status;
        if (status !== 200) {
            console.log(`Item Creation Error: status code ${status}`);
        }
        else {
            console.log(`Create Item Success: status code ${status}`);
        }
    };
    //override the form's submit method
    const itemForm = document.getElementById("item-form");
    itemForm.addEventListener("submit", (event) => {
        event.preventDefault();
        createItem();
    });
});
export {};
//# sourceMappingURL=itemManager.js.map