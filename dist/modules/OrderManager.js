import Order from "./Order.js";
import { v4 as uuidv4 } from 'uuid';
import FileManager from "./FileManager.js";
class OrderManager {
    //private static instance: OrderManager;
    // private constructor() {
    //     this.orders = [];
    // }
    /**
     * Returns the Singleton instance of OrderManager.
     */
    // public static getInstance() {
    //     if(!OrderManager.instance) {
    //         this.instance = new OrderManager();
    //     }
    //     return OrderManager.instance;
    // }
    constructor() {
        this.orders = [];
        this.file = new FileManager();
    }
    readOrdersFromFile() {
        this.orders = [];
        let ordersFromFile = this.file.readOrders();
        for (let i = 0; i < ordersFromFile.length; i++) {
            let nextOrder = ordersFromFile[i];
            this.add(nextOrder.username, nextOrder.email, nextOrder.address, nextOrder.order, nextOrder.date);
        }
    }
    saveOrders() {
        this.file.saveOrders(this.orders);
    }
    /**
     * Returns an array of all Orders in the system, both completed and in progress.
     * @returns Order[] - A collection of all Orders.
     */
    getOrders() {
        console.log(`OrderManager: returning all orders: ${JSON.stringify(this.orders)}`);
        return this.orders;
    }
    getOrdersFromUser(email) {
        let userOrders = [];
        for (let i = 0; i < this.orders.length; i++) {
            let nextOrder = this.orders[i];
            if (nextOrder.email == email) {
                // the Order has been found
                userOrders.push(nextOrder);
            }
        }
        return userOrders;
    }
    getOrder(orderId) {
        for (let i = 0; i < this.orders.length; i++) {
            let nextOrder = this.orders[i];
            if (nextOrder.orderId == orderId) {
                // the Order has been found
                nextOrder.total();
                return nextOrder;
            }
        }
        // no Order matches that ID
        return null;
    }
    completeOrder(orderId) {
        let order = this.getOrder(orderId);
        if (order != null) {
            return order.completeOrder();
        }
        return false;
    }
    /**
     * Used to add a new Order to OrderManager's collection of Orders.
     * @param username
     * @param email
     * @param address
     * @param items
     * @param date
     * @returns
     */
    add(username, email, address, items = [], date) {
        let orderId;
        let collision;
        do {
            orderId = uuidv4();
            collision = false;
            // make sure the orderId is unique before moving on
            if (this.getOrder(orderId)) {
                collision = true;
            }
        } while (collision);
        let newOrder;
        // if a Date is being passed in, then this is an existing Order being read in from a file
        if (date !== undefined) {
            newOrder = new Order(username, email, address, orderId, items, date);
        }
        else {
            // if no Date is supplied, then Order will generate a current Date on its own
            newOrder = new Order(username, email, address, orderId, items);
        }
        this.orders.push(newOrder);
        return newOrder;
    }
}
export default OrderManager;
//# sourceMappingURL=OrderManager.js.map