import bcrypt from 'bcrypt';

class User {

    username: string;
    private password: string;
    email: string;
    isAdmin: boolean;

    /**
     * Constructor for the User class. Takes a username and password, 
     * and hashes the password before storing it.
     * @param username String - The User's username
     * @param password String - The plaintext password the user typed in.
     * @param email String - The User's email address so they can be contacted.
     * @param isAdmin Boolean - A flag to distinguish between Client and Admin accounts.
     * @param isFromFile Boolean - This flag is used when restoring a User 
     *                  object that was saved to a file, in which case the password 
     *                  is already hashed and can be saved as is.
     */
    constructor(username: string, password: string, email: string, isAdmin: boolean, isFromFile: boolean) {
        this.username = username;
        if(isFromFile) {
            this.password = password;
        }
        else {
            this.password = bcrypt.hashSync(password, 10);
        }
        this.email = email;
        this.isAdmin = isAdmin;
    }

    /**
     * This takes a username and password, and compares the hash of the input password 
     * to the one stored by the User object. Returns true if both match.
     * @param uname String - The User's username
     * @param psw String - The User's plaintext password
     * @returns boolean - True if the username and password match, false otherwise.
     */
    authenticate(uname: string, psw: string): boolean {
        var usernameMatch = (uname === this.username);
        var passwordMatch = bcrypt.compareSync(psw, this.password);

        if(usernameMatch && passwordMatch) {
            console.log(`User: User authenticated.`);
            return true;
        }
        else {
            return false;
        }
    }
}

export default User;