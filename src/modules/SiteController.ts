import Item from './Item.js';
import OrderManager from './OrderManager.js';
import JWTManager from './JWTManager.js';
import UserManager from './UserManager.js';
import FileManager from './FileManager.js';
import { ClientItemOrder } from '../interfaces/ClientItemOrder.js';
import Order from './Order.js';
import ItemOrder from './ItemOrder.js';
import User from './User.js';

class SiteController {

    private static instance: SiteController;

    private items: Item[] = []; // holds an array of Item objects, each with data fields and an image src link
    private orders: OrderManager;
    private tokenManager: JWTManager;
    private users: UserManager;
    private file: FileManager = new FileManager();

    private constructor() {
        var temp = this.file.readItems();
        // if the Item storage is empty, "temp" will be undefined instead of an empty array
        if(temp !== null){
            this.items = temp;
        }
        this.tokenManager = JWTManager.getInstance();
        this.users = new UserManager();
        this.users.readUsersFromFile();
        this.orders = new OrderManager();
        this.orders.readOrdersFromFile();
    }

    public static getInstance(): SiteController {
        if(!SiteController.instance) {
            this.instance = new SiteController();
        }
        return this.instance;
    }

    public getUsers() {
        let userList = this.users.getUsers();
        let usersData = [];
        
        for(let i = 0; i < userList.length; i++) {
            let user = userList[i];
            let data = {
                username: user.username,
                email: user.email,
                isAdmin: user.isAdmin
            }
            usersData.push(data);
        }
        return usersData;        
    }

    /**
     * Returns a specific User account by its unique email, returning 
     * null if no account with that email exists.
     * @param email string - The User's unique email account.
     * @returns The User account or null if it is not found.
     */
    public getUserByUsername(username: string): Object | null {
        let user = this.users.getUserByUsername(username);
        if(user == null) {
            // no User found
            return null;
        }
        let data = {
            username: user.username,
            email: user.email,
            isAdmin: user.isAdmin
        }
        return data;
    }

    public getUserOrders(email: string): Order[] {
        return this.orders.getOrdersFromUser(email);
    }

    // These are the helper methods for the login path

    /**
     * Checks the input username and password against the credentials of the stored admin account. 
     * Returns the admin's JWT token if login is successful, and returns an empty string otherwise.
     * @param username string - The input username.
     * @param password string - The input password, not yet hashed.
     * @returns The admin account's JWT token if login is successful, or an empty string if it fails.
     */
    public loginAdmin(username: string, password: string): string {
        let admin = this.file.loadAdmin();
        if(admin == null) {
            console.log("Login Error: no Admin account on file.");
            throw new Error("No Admin account on file.");
        }
        let adminAccount = admin.authenticate(username, password);
        if(!adminAccount) {
            console.log("Login error: Admin username or password incorrect.")
            return "";
        }
        let adminToken = this.tokenManager.signToken(username, admin.email, admin.isAdmin);
        return adminToken;
    }

    /**
     * The login method for non-admin accounts. If the credentials are correct, it returns 
     * the user's JWT token to be sent down to the client.
     * @param username string - The user's username.
     * @param password string - The user's password.
     * @returns string - The user's JWT token.
     */
    public loginClient(username: string, password: string): string {
        console.log(`Logging in: ${username}, ${password}`);
        // this isn't the admin account. Login as a normal user.
        let newUser = this.users.authenticate(username, password);
        console.log(`Result: ${newUser}. Users: ${JSON.stringify(this.users.getUsers())}`);
        if(newUser != null) {
            // return their token
            return this.tokenManager.signToken(username, newUser.email, newUser.isAdmin);
        }
        return "";
    }

    public registerClient(username: string, password: string, email: string): boolean {
        let newUser = new User(username, password, email, false, false);
        let added: boolean = this.users.add(newUser);
        this.users.saveUsers();
        return added;
    }

    // the following methods deal with creating and managing Orders

    public processOrder(clientOrder: ClientItemOrder): boolean {
        // get the personal data of the client from their token
        let tokenData = this.tokenManager.getTokenData(clientOrder.token);
        if(!tokenData) {
            // invalid token
            console.log(`Error extracting token data. Token invalid.`);
            throw new Error("Invalid token.");
        }

        // this will hold the item orders
        let items: ItemOrder[] = [];

        // loop through each ItemID and quantity
        for(var i=0; i < clientOrder.items.length; i++) {
            let itemData = clientOrder.items[i];
            // get the full Item data from the Item's ID
            const foundItem = this.getItem(itemData.id);
            if(foundItem) {
                let newItemOrder = new ItemOrder(foundItem, itemData.quantity);
                items.push(newItemOrder);
            }
            else {
                // Item not found
                console.log(`Order error: Item not found with ID: ${itemData.id}`);
                return false;
            }
        }

        // the objects will be processed into proper ItemOrder objects and put here
        let order: Order = this.orders.add(tokenData.username, tokenData.email, clientOrder.address, items);
        this.orders.saveOrders();

        // if the code reaches this point, then the order has been successfully processed
        return true;
    }

    public getOrders(): Order[] {
        let outgoingOrders = this.orders.getOrders();
        console.log("Controller Orders: " + JSON.stringify(outgoingOrders));
        return outgoingOrders;
    }

    public getOrder(orderId: string): Order | null {
        return this.orders.getOrder(orderId);
    }

    /**
     * Marks an Order as complete for when shipping and payment are done.
     * @param orderId string - The Order's unique ID.
     * @returns boolean - True if successful, false if the Order can't be found or is already complete.
     */
    public completeOrder(orderId: string) {
        let completed: boolean = this.orders.completeOrder(orderId);
        this.orders.saveOrders();
        return completed;
    }

    public getItem = (id: string): Item | null => {
        for(let item of this.items) {
            if(item.id == id) {
                return item;
            }
        }
        return null;
    }

    public getAllItems(): string {
        return JSON.stringify(this.items);
    }

    public addItem(id: string, height: string, name: string, type: string, price: number, fileName: string) {

        var newItem = new Item(id, height, name, type, price, fileName);
        if(newItem) {
            // stop item creation if this iten's ID already exists
            for(let item of this.items) {
                if(item.id == newItem.id) {
                    // this item is a duplicate
                    console.log(`Duplicate Item sent: ${item.itemName}`);
                    return false;
                }
            }
            this.items.push(newItem);
            this.file.saveItems(this.items);
            console.log("New item added: " + JSON.stringify(newItem));
            return true;
        }
        else return false;        
    }

    public removeItem(id: string) {
        const item = this.getItem(id);
        
        if(!item) {
            // the Item could not be found
            console.log(`Controller: Item ${id} not found`);
            return false;
        }
        else {
            let index = -1;
            for(var i=0; i < this.items.length; i++) {
                let currentItem = this.items[i];
                if(currentItem.id == id) {
                    index = i;
                }
            }
            if(index > -1) {
                this.items.splice(index, 1);
                this.file.saveItems(this.items);
                // remove the image from the server
                this.file.removeImage('./public/images/' + item.imageSrc);
                console.log(`Controller: Item ${id} deleted.`);
                return true;
            }
        }
        return false;
    }
}

export default SiteController;