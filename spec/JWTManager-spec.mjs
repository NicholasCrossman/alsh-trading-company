import JWTManager from "../dist/modules/JWTManager.js"

describe("JSON Web Token Manager", () => {

    let manager;

    beforeEach( function() {
        manager = JWTManager.getInstance();
    });

    it("Tokens should prevent Clients from having Admin permissions", function() {
        let token = manager.signToken("James", "james@email.com", false);
        expect(manager.checkTokenPermissions(token)).toBeFalsy();
    })

    it("Tokens should preserve Admin permissions", function() {
        let token = manager.signToken("Yasha", "yasha@email.com", true);
        expect(manager.checkTokenPermissions(token)).toBeTruthy();
    })
})