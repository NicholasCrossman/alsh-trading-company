import Order from "../dist/modules/Order.js"
import ItemOrder from "../dist/modules/ItemOrder.js"
import Item from "../dist/modules/Item.js"

describe("Order of multiple item types", () => {
    let snowman = new Item("15207", "5", "Small Snowman", "Christmas Nesting Doll", 35, null);
    let snowmanOrder = new ItemOrder(snowman, 20);

    let owl = new Item("15209", "4", "Small Owl", "Nesting Doll", 32, null);
    let owlOrder = new ItemOrder(owl, 15);
    let items = [];
    items.push(snowmanOrder);
    items.push(owlOrder);

    let order = new Order("John Doe", "johndoe@email.com", "1234 South 1st Street, City, MI 12345", "8u243762438", items);

    it("Should properly calculate the total price of all items", function() {
        expect(order.price).toEqual(1180);
    })
})