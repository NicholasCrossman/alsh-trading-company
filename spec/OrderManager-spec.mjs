import Order from "../dist/modules/Order.js";
import OrderManager from "../dist/modules/OrderManager.js";
import Item from "../dist/modules/Item.js";
import ItemOrder from "../dist/modules/ItemOrder.js";

describe("Order Manager", () => {
    let manager = new OrderManager();
    //console.log(`Manager: ${JSON.stringify(manager)}`);
    let item1 = new Item("2436", "7", "Santa Doll", "Christmas Nesting Doll", 39, null);
    let item2 = new Item("2437", "2", "Blue Flower Box", "Lacquered Box", 18, null);
    let item3 = new Item("2439", "4", "Small Panda Doll", "Nesting Doll", 25, null);
    let item4 = new Item("2434", "8", "Green Santa", "Wooden Santa", 95, null);

    // order1 will be a previously completed order

    let itemOrder1 = new ItemOrder(item1, 15);
    let itemOrder2 = new ItemOrder(item2, 20);

    let orderItems1 = [];
    orderItems1.push(itemOrder1);
    orderItems1.push(itemOrder2);

    let order1 = manager.add("James", "james@email.com", "1234 West 247th Street, Jamestown, NJ", orderItems1);
    //console.log(`Order 1: ${JSON.stringify(order1)}`)

    order1.completeOrder();

    //console.log(`Order 1 completed: ${JSON.stringify(order1)}`)

    // order2 will not be completed

    let itemOrder3 = new ItemOrder(item3, 15);
    let itemOrder4 = new ItemOrder(item4, 8);

    let orderItems2 = [];
    orderItems2.push(itemOrder3);
    orderItems2.push(itemOrder4);

    let order2 = manager.add("Mark", "mark@email.com", "1244 West 247th Street, Jamestown, NJ", orderItems2);

    //console.log(`Orders: ${JSON.stringify(manager.getOrders())}`);
    
    it("Should return all orders", function() {
        expect(manager.getOrders()[0]).toEqual(order1);
    })

    it("Should return a single user's orders", function() {
        expect(manager.getOrdersFromUser("mark@email.com")[0]).toEqual(order2);
    })
})