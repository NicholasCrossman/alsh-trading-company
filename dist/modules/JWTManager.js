import fs from "fs";
import jwt from "jsonwebtoken";
import path from "path";
class JWTManager {
    /**
     * This is the constructor for a Singleton object. We only want one instance of this
     * class to ever be created, because it keeps a list of all active tokens under "activeKeys".
     */
    constructor() {
        this.privateKEY = fs.readFileSync(path.resolve("./src/keys/private.key"), 'utf-8');
        this.publicKEY = fs.readFileSync(path.resolve("./src/keys/public.key"), 'utf-8');
    }
    /**
     * This method actually returns the singleton.
     */
    static getInstance() {
        if (!JWTManager.instance) {
            JWTManager.instance = new JWTManager();
        }
        return JWTManager.instance;
    }
    /**
     * This method is used to issue a token when a User logs in. It stores the user's
     * username and a boolean for Admin permissions under the token's payload.
     * @param uname String - The User's username.
     * @param permissions Boolean - True if the User is an Admin, false otherwise.
     */
    signToken(uname, userEmail, permissions) {
        let payload = {
            username: uname,
            email: userEmail,
            isAdmin: permissions
        };
        let signOptions = {
            issuer: "Alsh Trading Co",
            subject: "Site User",
            audience: "www.alshtradingco.com",
            expiresIn: "12 h",
            algorithm: "RS256"
        };
        const token = jwt.sign(payload, this.privateKEY, signOptions);
        return token;
    }
    /**
     * This method returns true if the token specifies Admin permissions, false
     * if it specifies Client permissions, and null if the token is invalid.
     * @param token String - The string of the JWT token given to each User.
     */
    checkTokenPermissions(token) {
        let verifyOptions = {
            issuer: "Alsh Trading Co",
            subject: "Site User",
            audience: "www.alshtradingco.com",
            algorithms: ["RS256"]
        };
        try {
            const payload = jwt.verify(token, this.publicKEY, verifyOptions);
            return payload.isAdmin;
        }
        catch (err) {
            // the token is invalid
            console.log("Token verification error: " + err);
            return null;
        }
    }
    getTokenData(token) {
        let verifyOptions = {
            issuer: "Alsh Trading Co",
            subject: "Site User",
            audience: "www.alshtradingco.com",
            algorithms: ["RS256"]
        };
        try {
            return jwt.verify(token, this.publicKEY, verifyOptions);
        }
        catch (err) {
            // the token is invalid
            console.log("Token verification error: " + err);
            return null;
        }
    }
}
export default JWTManager;
//# sourceMappingURL=JWTManager.js.map