import User from '../dist/modules/User.js'

describe("User", () => {

    describe("Admin account", () => {
        it("Should allow the Admin to log in with the right username and password", function() {
            let admin = new User("Yasha", "test", "admin@email.com", true, false);
            let returnedUser = admin.authenticate("Yasha", "test");
            expect(returnedUser).toBeTrue();
        })
        it("Should reject an incorrect username or password", function() {
            let admin = new User("Yasha", "test", "admin@email.com", true, false);
            let returnedUser = admin.authenticate("Yasha", "testy");
            expect(returnedUser).toBeFalse();
        })
    })
})
