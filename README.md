# alsh-trading-company

This is a website for the Alsh Trading Company, meant to serve as an online catalog of merchandise.

## Languages

The project was initially written in Node.js with JavaScript, but was refactored completely to TypeScript.  
The server is a REST API server centered on `server.ts` with Node.js and Express, with the front-end written in  
Angular.  

## Where is the front end?

It's now a separate repository, written in Angular. You can find it on my page as `alsh-trading-co-frontend`.
## Unit Testing

The project has unit tests written in Jasmine, which test the output `.js` files after the TypeScript has been  
compiled to JavaScript.

## Next Steps

The front-end is now underway. Adding and deleting Items now works using the Angular UI, but creating an 
Order of Items still needs to be implemented. Login functionality is next.