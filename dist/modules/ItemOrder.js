class ItemOrder {
    constructor(item, quantity) {
        this.item = item;
        this.totalPrice = 0;
        // the quantity cannot be zero or less
        if (quantity <= 0) {
            console.log("Error: quantity of ItemOrder must be a positive nonzero number.");
            this.quantity = 0;
        }
        else {
            this.quantity = quantity;
            this.totalPrice = this.item.price * this.quantity;
        }
    }
}
export default ItemOrder;
//# sourceMappingURL=ItemOrder.js.map