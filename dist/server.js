import express from 'express';
import multer from 'multer';
import path from 'path';
import JWTManager from './modules/JWTManager.js';
import SiteController from './modules/SiteController.js';
import cors from 'cors';
const __dirname = path.resolve("./");
const storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './public/images');
    },
    filename: function (req, file, callback) {
        callback(null, file.originalname);
    }
});
const upload = multer({ storage: storage });
const app = express();
const router = express.Router();
const PORT = 3000;
let manager;
let controller;
app.use(cors());
app.use(express.json());
let dir = path.join(__dirname, "public/images");
app.use(express.static(dir));
app.use(express.urlencoded({ extended: true }));
// the server reads data from storage when it starts up. The file is a .json file named saved-data.json
// this is stored under /src/data
var server = app.listen(PORT, () => {
    console.log(`Express server currently running on port ${PORT}`);
    manager = JWTManager.getInstance();
    controller = SiteController.getInstance();
});
// this is the Login method
app.post("/login", (req, resp) => {
    // get the request data with the username and password
    const username = req.body["username"];
    const password = req.body["password"];
    console.log(`Login recieved: ${username}, ${password}`);
    try {
        let adminToken = controller.loginAdmin(username, password);
        if (adminToken == "") {
            // the Admin username or password was incorrect
            // try logging in as a client
            let userToken = controller.loginClient(username, password);
            if (userToken == "") {
                // The username or password was incorrect
                resp.status(401).send({
                    message: "Username or password incorrect."
                });
            }
            else {
                console.log("Client Login successful.");
                // The User has successfully logged in. Send down their token.
                return resp.status(200).json({
                    token: userToken,
                    username: username,
                    isAdmin: false
                });
            }
        }
        else {
            // the Admin has successfully logged on
            console.log("Admin Login successful.");
            return resp.status(200).json({
                token: adminToken,
                username: username,
                isAdmin: true
            });
        }
    }
    catch (e) {
        // An exception means there was an error loading the Admin account from file
        return resp.status(500).send({
            message: e.message
        });
    }
});
app.post('/register', (req, resp) => {
    // get the request data with the submitted username and password
    const username = req.body["username"];
    const email = req.body["email"];
    const password = req.body["password"];
    let isAdded = controller.registerClient(username, password, email);
    if (isAdded) {
        return resp.status(200).send({
            message: "User creation successful!\nPlease log in with your new account."
        });
    }
    // if the code reaches this, the operation failed
    return resp.status(401).send({
        message: `Error: An account with ${email} already exists.\nPlease log in or choose a different email.`
    });
});
app.get('/users', (req, resp) => {
    let users = controller.getUsers();
    return resp.status(200).send({
        users: JSON.stringify(users)
    });
});
// this GET returns a User by their username
app.get('/users/:username', (req, resp) => {
    let username = String(req.params.username);
    let user = controller.getUserByUsername(username);
    if (user) {
        // the User has been found, return the User object
        return resp.status(200).send({
            user: JSON.stringify(user)
        });
    }
    // no User found
    return resp.status(500).send({
        message: `Error: No User ${username} found.`
    });
});
app.get('/users/:email/order', (req, resp) => {
    const email = String(req.params.email);
    let orders = controller.getUserOrders(email);
    // if the array has values in it, send it normally
    if (orders.length) {
        return resp.status(200).send({
            orders: JSON.stringify(orders)
        });
    }
    // if the array is empty, then no orders were found
    return resp.status(500).send({
        message: `Error: No Orders with email: ${email} found.`
    });
});
// this GET returns an Array of all Item objects held on the server
app.get('/items', (req, resp) => {
    //console.log("Items: " + JSON.stringify(items));
    return resp.json(controller.getAllItems());
});
// this GET retrieves information on a specific item
app.get("/items/:id", (req, resp) => {
    const id = String(req.params.id);
    const foundItem = controller.getItem(id);
    if (!foundItem) {
        return resp.status(500).send("Error: Item not found.");
    }
    else {
        return resp.json(JSON.stringify(foundItem));
    }
});
// this POST is a form upload that creates a new Item object.
// The user inputs an Item's item-name, item-type, price, and an image of the item.
// The text fields are saved in an Item object, and the image is saved under public/images
// The image's new URL is saved in the Item under imageSrc
app.post("/items", upload.single('photo'), (req, resp) => {
    if (!req.file) {
        console.log("No file received.");
        return resp.status(500).send({
            success: false
        });
    }
    else {
        const requestData = req.body;
        if (!requestData) {
            console.log("No data received.");
            return resp.status(500).send({
                success: false
            });
        }
        let id = requestData["item-id"];
        let height = requestData["item-height"];
        let name = requestData["item-name"];
        let type = requestData["item-type"];
        let price = requestData["price"];
        let fileName = req.file.originalname;
        let added = controller.addItem(id, height, name, type, price, fileName);
        if (!added) {
            // an error occurred adding the item
            return resp.status(500).send({
                message: `Error: could not add Item: ${name}`
            });
        }
        else {
            return resp.status(200).send({
                message: `Success: Item ${name} added.`
            });
        }
        // console.log(newItem);
        // console.log(req.file);
    }
});
// the following methods deal with Orders
// this GET returns all Orders in the system
app.get('/orders', (req, resp) => {
    console.log("Resource entered.");
    let orders = controller.getOrders();
    console.log("Orders outgoing:" + JSON.stringify(orders));
    // if the array has values in it, send it normally
    if (orders.length > 0) {
        return resp.status(200).send({
            orders: JSON.stringify(orders)
        });
    }
    // if the array is empty, then no orders were found
    return resp.status(500).send({
        message: `Error: Orders not found.`
    });
});
// this GET returns a specific Order by its unique OrderID
app.get('/orders/:id', (req, resp) => {
    const id = req.params.id;
    let order = controller.getOrder(id);
    if (order) {
        return resp.status(200).send({
            orders: JSON.stringify(order)
        });
    }
    return resp.status(500).send({
        message: `Error: an Order with id: ${id} was not found.`
    });
});
app.post('/orders/complete/:id', (req, resp) => {
    let orderId = req.params.id;
    let completed = controller.completeOrder(orderId);
    if (completed) {
        return resp.status(200).send({
            message: `Success: Order ${orderId} marked as complete.`
        });
    }
    return resp.status(500).send({
        message: `Error: Order ${orderId} can't be found, or is already complete.`
    });
});
// This POST lets a Client submit an Order of Items. Each Item has a specified quantity.
// The order contains the client's shipping information, and is authenticated with a JWT token.
app.post("/orders", (req, resp) => {
    // The Client sends an order of the form specified by ClientItemOrder
    let clientOrder = req.body;
    // let clientOrder: ClientItemOrder = {
    //     token: req.body.token,
    //     date: req.body.date,
    //     address: req.body.address,
    // }
    console.log(`Order Recieved: ${JSON.stringify(clientOrder)}`);
    let orderProcessed = controller.processOrder(clientOrder);
    if (orderProcessed) {
        // the order was processed successfully
        return resp.status(200).send({
            message: "Success: Order processed successfully."
        });
    }
    else {
        // there was an error retrieving an Item
        return resp.status(500).send({
            message: "Error: A requested Item was not found."
        });
    }
});
// this DELETE method removes the item with the specified ItemID, which is unique
app.delete("/items/:id", (req, resp) => {
    const id = req.params.id;
    const removed = controller.removeItem(id);
    if (!removed) {
        console.log(`Server: Item ${id} not deleted: Removed ${removed}`);
        return resp.status(500).send("Error: Item not found.");
    }
    else {
        return resp.status(200).send({
            message: `Item ${id} deleted.`
        });
    }
});
//# sourceMappingURL=server.js.map