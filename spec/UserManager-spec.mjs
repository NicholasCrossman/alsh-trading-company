import User from "../dist/modules/User.js";
import UserManager from "../dist/modules/UserManager.js";

describe("User Manager", () => {

    describe("The user should be able to login through the UserManager", function() {
        let user = new User("user", "test", "user@email.com", false, false);
        let userManager = new UserManager();
        userManager.add(user);

        let loginReturn;

        beforeEach(function() {
            loginReturn = userManager.authenticate("user", "test");
        })

        it("The username should match", function() {
            expect(loginReturn.username).toEqual(user.username);
        })
        
        it("The email should match", function() {
            expect(loginReturn.email).toEqual(user.email);
        })

        it("The permissions should match", function() {
            expect(loginReturn.isAdmin).toEqual(user.isAdmin);
        })
    })

    describe("The User manager should prevent accounts with duplicate emails or usernames from being created", function() {
        let user1 = new User("user1", "test1", "user1@email.com", false, false);
        let userDuplicateEmail = new User("user2", "test2", "user1@email.com", false, false);
        let userDuplicateUsername = new User("user1", "test2", "user2@email.com", false, false);

        let uniqueUser = new User("newUser", "test3", "newuser@email.com", false, false);
        let userManager = new UserManager();
        userManager.add(user1);

        it("Should reject a duplicate email", function() {
            expect(userManager.add(userDuplicateEmail)).toBeFalse();
        })

        it("Should reject a duplicate username", function() {
            expect(userManager.add(userDuplicateUsername)).toBeFalse();
        })

        it("Should allow a new User with unique username and email", function() {
            expect(userManager.add(uniqueUser)).toBeTrue();
        })
    })
})