import Item from "./Item.js"

class ItemOrder {

    quantity: number;
    totalPrice: number = 0;

    constructor(public item: Item, quantity: number) {
        // the quantity cannot be zero or less
        if(quantity <= 0) {
            console.log("Error: quantity of ItemOrder must be a positive nonzero number.");
            this.quantity = 0;
        }
        else {
            this.quantity = quantity;
            this.totalPrice = this.item.price * this.quantity;
        }
    }
}

export default ItemOrder;