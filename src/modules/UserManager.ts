import FileManager from "./FileManager.js";
import User from "./User.js";

class UserManager {
    private users: User[];
    private file: FileManager;
    // private static instance: UserManager;

    // private constructor() {
    //     this.users = [];
    // }

    // public static getInstance(): UserManager {
    //     if(!UserManager.instance) {
    //         UserManager.instance = new UserManager();
    //     }
    //     return UserManager.instance;
    // }

    constructor() {
        this.users = [];
        this.file = new FileManager();
    }

    /**
     * Reads in Users from the 'saved-users.json' file. Returns true if successful, 
     * and returns false if the array is empty.
     * @returns boolean - True if successful, false if the array of Users is empty.
     */
    readUsersFromFile(): boolean {
        let usersFromFile = this.file.readUsers();
        if(usersFromFile.length > 0) {
            this.users = usersFromFile;
            return true;
        }
        return false;
    }

    saveUsers() {
        this.file.saveUsers(this.users);
    }

    getUsers(): User[] {
        return this.users;
    }

    getUserByUsername(username: string): User | null {
        for(let i = 0; i < this.users.length; i++) {
            let nextUser = this.users[i];
            if(nextUser.username === username) {
                return nextUser;
            }
        }
        return null;
    }

    add(user: User): boolean {
        // make sure the email and username are unique
        let email = user.email;
        let username = user.username;
        for(var i=0; i < this.users.length; i++) {
            let nextUser: User = this.users[i];
            if(nextUser.email === email || nextUser.username === username) {
                // this user already exists. Ask for a different email or username.
                return false;
            }
        }
        this.users.push(user);
        return true;
    }

    authenticate(username: string, password: string): User | null {
        for(var i=0; i < this.users.length; i++) {
            let nextUser = this.users[i];
            if(nextUser.username === username) {
                // this user exists. Now try to authenticate them.
                let authenticated: boolean = nextUser.authenticate(username, password);
                if(authenticated) {
                    return nextUser;
                }
            }
        }
        // if the code reaches this point, the User does not exist
        console.log(`User ${username} not found.`);
        return null;
    }
}

export default UserManager;