import fs from "fs";
import jwt from "jsonwebtoken";
import path from "path";

class JWTManager {

    private privateKEY: jwt.Secret;
    private publicKEY: jwt.Secret;
    private static instance: JWTManager;

    /**
     * This is the constructor for a Singleton object. We only want one instance of this 
     * class to ever be created, because it keeps a list of all active tokens under "activeKeys".
     */
    private constructor() {
        this.privateKEY = fs.readFileSync(path.resolve("./src/keys/private.key"), 'utf-8');
        this.publicKEY = fs.readFileSync(path.resolve("./src/keys/public.key"), 'utf-8');
    }

    /**
     * This method actually returns the singleton.
     */
    public static getInstance(): JWTManager {
        if(!JWTManager.instance) {
            JWTManager.instance = new JWTManager();
        }
        return JWTManager.instance;
    }

    /**
     * This method is used to issue a token when a User logs in. It stores the user's 
     * username and a boolean for Admin permissions under the token's payload.
     * @param uname String - The User's username.
     * @param permissions Boolean - True if the User is an Admin, false otherwise.
     */
    public signToken(uname: string, userEmail: string, permissions: boolean): string {
        let payload: TokenPayload = {
            username: uname,
            email: userEmail,
            isAdmin: permissions
        };

        let signOptions: jwt.SignOptions = {
            issuer: "Alsh Trading Co",
            subject: "Site User",
            audience: "www.alshtradingco.com",
            expiresIn: "12 h",
            algorithm: "RS256"
        };

        const token = jwt.sign(payload, this.privateKEY, signOptions);

        return token;
    }



    /**
     * This method returns true if the token specifies Admin permissions, false 
     * if it specifies Client permissions, and null if the token is invalid.
     * @param token String - The string of the JWT token given to each User.
     */
    public checkTokenPermissions(token: string) {
        let verifyOptions: jwt.VerifyOptions = {
            issuer: "Alsh Trading Co",
            subject: "Site User",
            audience: "www.alshtradingco.com",
            algorithms: ["RS256"]
        };

        try {
            const payload = jwt.verify(token, this.publicKEY, verifyOptions) as TokenPayload;
            return payload.isAdmin;
        }
        catch(err) {
            // the token is invalid
            console.log("Token verification error: " + err);
            return null;
        }
    }

    public getTokenData(token: string) {
        let verifyOptions: jwt.VerifyOptions = {
            issuer: "Alsh Trading Co",
            subject: "Site User",
            audience: "www.alshtradingco.com",
            algorithms: ["RS256"]
        };

        try {
            return jwt.verify(token, this.publicKEY, verifyOptions) as TokenPayload;
        }
        catch(err) {
            // the token is invalid
            console.log("Token verification error: " + err);
            return null;
        }
    }
}

interface TokenPayload {
    username: string;
    email: string;
    isAdmin: boolean;
}

export default JWTManager;