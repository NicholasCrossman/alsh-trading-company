import ItemOrder from "../dist/modules/ItemOrder.js"
import Item from "../dist/modules/Item.js"

describe("Quantity of an individual Item", () => {
    let snowman = new Item("15207", "5", "Small Snowman", "Christmas Nesting Doll", 35, null);
    let snowmanOrder = new ItemOrder(snowman, 20);

    it("Should calculate the total price", function() {
        expect(snowmanOrder.totalPrice).toEqual(700);
    })
})